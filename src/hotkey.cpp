/*
    'Hotkey' is a small utility that provides global shortcuts for often needed actions.
    See ReadMe.md for more information.

    Copyright � 2020-2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT

    MSDN remarks:
	    When a key is pressed, the system looks for a match against all hot keys.
	    Upon finding a match, the system posts the WM_HOTKEY message to the message queue of the
	    window with which the hot key is associated. If the hot key is not associated with a window,
	    then the WM_HOTKEY message is posted to the thread associated with the hot key.

	    This function cannot associate a hot key with a window created by another thread.

	    RegisterHotKey fails if the keystrokes specified for the hot key have already been registered by another hot key.

	    If a hot key already exists with the same hWnd and id parameters, it is maintained along with the new hot key.
	    The application must explicitly call UnregisterHotKey to unregister the old hot key.

    Virtual-Key Codes: <https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes>
*/

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <vector>
#include <windows.h>
#include <tchar.h>
#include "version.h"
#include "resource.h"

UINT WM_TASKBAR = 0;
HMENU hmenu;
NOTIFYICONDATA notifyIconData;

// Get a notification in case the Windows Explorer/Shell crashes and re-launches the taskbar.
// Without this	the application could remain inaccessible for the user!
const static UINT WM_TASKBARCREATED = RegisterWindowMessage(L"TaskbarCreated");

void minimize (HWND hwnd)
{
	ShowWindow(hwnd, SW_HIDE);
}

void restore (HWND hwnd)
{
	ShowWindow(hwnd, SW_SHOW);
}

void InitNotifyIconData (HWND hwnd)
{
	memset(&notifyIconData, 0, sizeof(NOTIFYICONDATA));

	notifyIconData.cbSize = sizeof(NOTIFYICONDATA);
	notifyIconData.hWnd = hwnd;
	notifyIconData.uID = ID_TRAY_APP_ICON;
	notifyIconData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_INFO;
	notifyIconData.uCallbackMessage = WM_SYSICON; // Set up our invented Windows Message
	notifyIconData.hIcon = (HICON) LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(ICON_1));
	
	std::wstring tip(APP_NAME);
	tip.append(L" ");
	tip.append(VERSION_STRING);
	tip.append(L"\n");
	tip.append(APP_URL);
	_tcscpy(notifyIconData.szTip, tip.c_str());
}

// -------------------------------------------------------------------------------------------------

/* The keys that must be pressed in combination with the key specified
   by the virtual_key_code parameter in order to generate the WM_HOTKEY message.

The modifiers parameter can be a combination of the following values:
- MOD_ALT     (0x0001) - Either ALT key must be held down. 
- MOD_CONTROL (0x0002) - Either CTRL key must be held down. 
- MOD_SHIFT   (0x0004) - Either SHIFT key must be held down. 
- MOD_WIN     (0x0008) - Either WINDOWS key was held down -- but, better not use this key:
                         While it works (mostly), it's best practice not to use keyboard shortcuts that involve the WINDOWS key:
                         They are reserved for use by the operating system! */
int const modifiers_FullTimestamp    = MOD_ALT | MOD_CONTROL;
int const modifiers_DateOnly         = MOD_CONTROL | MOD_SHIFT;
int const modifiers_lowercase_umlaut = MOD_CONTROL;
int const modifiers_uppercase_umlaut = MOD_CONTROL | MOD_SHIFT;

int const special_modifiers = MOD_NOREPEAT;
	/* The special modifiers parameter can be a combination of the following values:
		MOD_NOREPEAT (0x4000) - Changes the hotkey behavior so that the keyboard auto-repeat does not yield multiple hotkey notifications.
		                        (This flag is not supported under Windows Vista.) */

int const virtualkeycode_T = 0x54; /* key 'T'*/

int const hotkey_id_date = 1000;
int const hotkey_id_full = 2000;
int const hotkey_id_lowercase_a_umlaut = 3001;
int const hotkey_id_uppercase_a_umlaut = 3002;
int const hotkey_id_lowercase_o_umlaut = 3003;
int const hotkey_id_uppercase_o_umlaut = 3004;
int const hotkey_id_lowercase_u_umlaut = 3005;
int const hotkey_id_uppercase_u_umlaut = 3006;
int const hotkey_id_eszett             = 3007;


std::string getDateAsText ()
{
	std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::tm* now_tm = std::localtime(&time);

	char buffer[16];
	std::strftime(buffer, sizeof buffer, "%Y-%m-%d", now_tm);

	std::string str(buffer);
	return str;
}


std::string getTimeAsText ()
{
	std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::tm* now_tm = std::localtime(&time);

	char buffer[10];
	std::strftime(buffer, sizeof buffer, "%H:%M:%S", now_tm);

	std::string str(buffer);
	return str;
}


void sendText (int const mod)
{
	std::string msg;
	
	if (mod == modifiers_FullTimestamp)
	{
		msg = getDateAsText();
	}
	else if (mod == modifiers_DateOnly)
	{
		msg = getDateAsText() + " " + getTimeAsText();
	}

	// Copy & Paste from StackOverflow...
	std::vector<INPUT> vec;
	
	for (auto ch : msg)
    {
		INPUT input = { 0 };
		input.type = INPUT_KEYBOARD;
		input.ki.dwFlags = KEYEVENTF_UNICODE;
		input.ki.wScan = ch;
		vec.push_back(input);
		input.ki.dwFlags |= KEYEVENTF_KEYUP;
		vec.push_back(input);
    }

	SendInput(static_cast<UINT>(vec.size()), vec.data(), sizeof(INPUT));
}


void sendUmlautCharacter (unsigned long long character, int const mod)
{
    WORD ch = 0; // Will hold the hexadecimal value of an unicode character.

    if      (character == hotkey_id_lowercase_a_umlaut && mod == modifiers_lowercase_umlaut) { ch = 0x00E4; } // �
	else if (character == hotkey_id_uppercase_a_umlaut && mod == modifiers_uppercase_umlaut) { ch = 0x00C4; } // �
    else if (character == hotkey_id_lowercase_o_umlaut && mod == modifiers_lowercase_umlaut) { ch = 0x00F6; } // �
	else if (character == hotkey_id_uppercase_o_umlaut && mod == modifiers_uppercase_umlaut) { ch = 0x00D6; } // �
    else if (character == hotkey_id_lowercase_u_umlaut && mod == modifiers_lowercase_umlaut) { ch = 0x00FC; } // �
	else if (character == hotkey_id_uppercase_u_umlaut && mod == modifiers_uppercase_umlaut) { ch = 0x00DC; } // �
    else if (character == hotkey_id_eszett && mod == modifiers_lowercase_umlaut)             { ch = 0x00DF; } // �

	INPUT input = { 0 };
    input.type = INPUT_KEYBOARD;
    input.ki.dwFlags = KEYEVENTF_UNICODE;
    input.ki.wVk = 0;
    input.ki.wScan = ch;

	SendInput(1, &input, sizeof(INPUT));
}


// Window Procedure: To handle the messages from the queue.
LRESULT CALLBACK WinProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	if (msg == WM_TASKBARCREATED && !IsWindowVisible(hwnd))
	{
		minimize(hwnd);
		return 0;
	}
	
	switch (msg)
	{
		case WM_ACTIVATE:
		{
			Shell_NotifyIcon(NIM_ADD, &notifyIconData);
			break;
		}
	
		case WM_CREATE: // Sent prior to the WM_CREATE message when a window is first created.
		{
			ShowWindow(hwnd, SW_HIDE);
			hmenu = CreatePopupMenu();
			AppendMenu(hmenu, MF_STRING, ID_TRAY_ABOUT, TEXT("About..."));
			AppendMenu(hmenu, MF_STRING, ID_TRAY_EXIT, TEXT("Exit"));
			
			if (!(RegisterHotKey(hwnd, hotkey_id_date, modifiers_FullTimestamp | special_modifiers, virtualkeycode_T)
			   && RegisterHotKey(hwnd, hotkey_id_full, modifiers_DateOnly | special_modifiers, virtualkeycode_T)
               && RegisterHotKey(hwnd, hotkey_id_lowercase_a_umlaut, modifiers_lowercase_umlaut | special_modifiers, VK_F1)
               && RegisterHotKey(hwnd, hotkey_id_uppercase_a_umlaut, modifiers_uppercase_umlaut | special_modifiers, VK_F1)
               && RegisterHotKey(hwnd, hotkey_id_lowercase_o_umlaut, modifiers_lowercase_umlaut | special_modifiers, VK_F2)
               && RegisterHotKey(hwnd, hotkey_id_uppercase_o_umlaut, modifiers_uppercase_umlaut | special_modifiers, VK_F2)
               && RegisterHotKey(hwnd, hotkey_id_lowercase_u_umlaut, modifiers_lowercase_umlaut | special_modifiers, VK_F3)
               && RegisterHotKey(hwnd, hotkey_id_uppercase_u_umlaut, modifiers_uppercase_umlaut | special_modifiers, VK_F3)
               && RegisterHotKey(hwnd, hotkey_id_eszett            , modifiers_lowercase_umlaut | special_modifiers, VK_F4)
               ))
			{
				OutputDebugString(L"Error: Couldn't register all hotkeys! Is one of them maybe already in use?\n");
				std::exit(1);
			}
			break;
		}

		// (for the Tray Icon)
		case WM_SYSCOMMAND:
		{
			/* In WM_SYSCOMMAND messages, the four low-order bits of the wParam parameter
			are used internally by the system. To obtain the correct result when testing the value of wParam,
			an application must combine the value 0xFFF0 with the wParam value by using the bitwise AND operator. */

			switch (wparam & 0xFFF0)
			{
				case SC_MINIMIZE: // Fall through!
				case SC_CLOSE:
					minimize(hwnd);
					return 0;
					break;
			}
			break;
		}

		// (for the Tray Icon) Our user defined WM_SYSICON message.
		case WM_SYSICON:
		{
			switch (wparam)
			{
				case ID_TRAY_APP_ICON:
					SetForegroundWindow(hwnd);
					break;
			}

			if (lparam == WM_LBUTTONUP)
			{
				restore(hwnd);
			}
			else if (lparam == WM_RBUTTONDOWN) 
			{
				// Get current mouse position.
				POINT curPoint ;
				GetCursorPos(&curPoint);
				SetForegroundWindow(hwnd);

				// TrackPopupMenu blocks the app until TrackPopupMenu returns
				UINT clicked = TrackPopupMenu(hmenu,TPM_RETURNCMD | TPM_NONOTIFY, curPoint.x, curPoint.y, 0, hwnd, NULL);
				            
				SendMessage(hwnd, WM_NULL, 0, 0); // Send benign message to window to make sure the menu goes away.
				
				if (clicked == ID_TRAY_EXIT)
				{
					Shell_NotifyIcon(NIM_DELETE, &notifyIconData);
					PostQuitMessage(0) ;
				}
				else if (clicked == ID_TRAY_ABOUT)
				{
					std::wstring caption(L"About ");
					caption.append(APP_NAME);

					std::wstring text(APP_NAME);
					text.append(L" ");
					text.append(VERSION_STRING);
					text.append(L"\n");
					text.append(APP_DESCRIPTION);
					text.append(L"\n");
					text.append(APP_URL);
					text.append(L"\n");
					text.append(COPYRIGHT_NOTICE);

					MessageBox(hwnd, text.c_str(), caption.c_str(), MB_OK | MB_ICONINFORMATION);
				}
			}
			break;
		}

		// (for the Tray Icon) Intercept the hit-test message.
		case WM_NCHITTEST:
		{
			UINT uHitTest = static_cast<UINT>(DefWindowProc(hwnd, WM_NCHITTEST, wparam, lparam));
			
			if (uHitTest == HTCLIENT)
				return HTCAPTION;
			else
				return uHitTest;
		}
		
		case WM_HOTKEY:
		{
			switch (wparam)
			{
				case hotkey_id_date: [[fallthrough]];
				case hotkey_id_full:
                {
                    sendText(LOWORD(lparam));
                    break;

                    //// The low-order word specifies the keys that were to be pressed in
                    //// combination with the key specified by the high-order word to generate the WM_HOTKEY message.
                    //// This word can be one or more of the following values: MOD_ALT | MOD_CONTROL | MOD_SHIFT | MOD_WIN.
                    //switch (LOWORD(lparam))
                    //{
                    //	case modifiers_FullTimestamp:
                    //		sendText(modifiers_FullTimestamp);
                    //		break;
                    //}

                    //// The high-order word specifies the virtual key code of the hot key.
                    //switch (HIWORD(lparam))
                    //{
                    //	case virtualkeycode_T:
                    //		break;
                    //}
                }

                case hotkey_id_lowercase_a_umlaut: [[fallthrough]];
                case hotkey_id_uppercase_a_umlaut: [[fallthrough]];
                case hotkey_id_lowercase_o_umlaut: [[fallthrough]];
                case hotkey_id_uppercase_o_umlaut: [[fallthrough]];
                case hotkey_id_lowercase_u_umlaut: [[fallthrough]];
                case hotkey_id_uppercase_u_umlaut: [[fallthrough]];
                case hotkey_id_eszett:
                {
                    sendUmlautCharacter(wparam, LOWORD(lparam));
                    break;
                }
			}
			break;
		}
		
		case WM_CLOSE:
			minimize(hwnd); // (for the Tray Icon)
			return 0;
			break;

		case WM_DESTROY:
		{
            if (!(UnregisterHotKey(hwnd, hotkey_id_date)
			   && UnregisterHotKey(hwnd, hotkey_id_full)
               && UnregisterHotKey(hwnd, hotkey_id_lowercase_a_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_uppercase_a_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_lowercase_o_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_uppercase_o_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_lowercase_u_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_uppercase_u_umlaut)
               && UnregisterHotKey(hwnd, hotkey_id_eszett))
               )
			{
				OutputDebugString(L"Error: Couldn't unregister hotkeys!\n");
			}

			PostQuitMessage(0);
			break;
		}
		
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}


// -------------------------------------------------------------------------------------------------

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
	HWND hwnd;
	LPCWSTR windowClassName = L"Hotkey";
	
	WNDCLASSEX wclass = {0};
    wclass.cbSize = sizeof(WNDCLASSEX);
    wclass.style = CS_DBLCLKS; /* Catch double-clicks */
    wclass.lpfnWndProc = WinProc;
    wclass.cbClsExtra = 0;
    wclass.cbWndExtra = 0;
    wclass.hInstance = hInstance;
    wclass.hCursor = NULL;
    wclass.hbrBackground = NULL;
    wclass.lpszMenuName = NULL;
    wclass.lpszClassName = windowClassName;
	wclass.hIcon = LoadIcon (GetModuleHandle(NULL), MAKEINTRESOURCE(ICON_1));
    wclass.hIconSm = LoadIcon (GetModuleHandle(NULL), MAKEINTRESOURCE(ICON_1));
    
	if (!RegisterClassEx(&wclass)) { return 0; }

	/* A window is needed, even though we're only interested in the icon in Taskbar Notification Area.
	So, we create a "message-only window" without any visible features.
	Note that Message-Only-Windows do not receive broadcast messages. */
    hwnd = CreateWindowEx(0, windowClassName, NULL, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);

	if (hwnd == NULL)
	{
		return 0;
	}
	
    InitNotifyIconData(hwnd);
    ShowWindow (hwnd, nCmdShow);
	
	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
