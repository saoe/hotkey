# Hotkey

_Hotkey_ is a small utility for Windows that provides global shortcuts for often needed actions.

Currently, there are only a few (and hardwired!) hotkeys implemented, and they all insert some kind
of text (string or character) into any kind of text area (e.g. a file, an input field, a Word document, etc.).

When the program runs (check for the icon in the status area), these hotkey combinations are available:

Hotkey combination | Effect
-------------------|---------------------------------------
Ctrl + Alt   + T   | Insert a date-only timestamp (e.g. 2020-05-28)
Ctrl + Shift + T   | Insert a full timestamp (e.g. 2020-05-28 18:11:25)
Ctrl + F1          | Insert a lowercase german a-umlaut (ä)
Ctrl + Shift + F1  | Insert an uppercase german a-umlaut (Ä)
Ctrl + F2          | Insert a lowercase german o-umlaut (ö)
Ctrl + Shift + F2  | Insert an uppercase german o-umlaut (Ö)
Ctrl + F3          | Insert a lowercase german u-umlaut (ü)
Ctrl + Shift + F3  | Insert an uppercase german u-umlaut (Ü)
Ctrl + F4          | Insert a german Eszett (ß)

I may or may not extend this tool in the future, depeding on my needs, mood and/or time... :-)

But in the meantime, you could use this also as a starting-point for your own customization,
or for peeking on how to implement global hotkeys under Windows, or how to make use of a simple _tray icon_
(i.e. an icon in the [taskbar notification area](https://en.wikipedia.org/wiki/Taskbar#Taskbar_elements)).

So long, and happy coding!

## Implementation

- Written in C++, against the Win32-API; tested under Windows 10.
- Built with [Visual Studio 2019 (Community Edition)](https://visualstudio.microsoft.com/) and [CMake](https://cmake.org/).
- The program's icon was created with [Paint.NET](https://www.getpaint.net/) and [IcoFX 1.6.4](https://icofx.ro/).

## Download executable file

You can get a ready-to-use binary of this program as a zipped file from the "Downloads" section (see link in the left pane).

## Building from source

Requires [CMake](https://cmake.org/)!

1. Configuration step:  
   `C:\project\path> cmake -S src -B _build`

2. Build step:  

   * Either via CMake:  
   `C:\project\path> cmake --build _build --target INSTALL --config Release`  

   * Or via Visual Studio:  
   Open the generated _\*.sln_ from step 1 (can be found in `C:\project\path\_build`) and then build the project _INSTALL_.

The generated executable file can then be found in `C:\project\path\_build\_installed` by default.
   
There are no further dependencies on this file, so it's portable and you can simply move it
from the above mentioned output directory to any other location that suits you better and run it from there.

## History

- 2020-04-22: Created (to insert timestamps).
- 2020-05-27: Further improvements and 'tray icon' added.
- 2020-05-28: Published on my site and BitBucket.
- 2021-10-28: Version 1.1: Added hotkeys for german umlauts.

----------------------------------------------------------------------------------------------------

## Tracker (Issues & Todos)

- Idea: Use a configuration file or command-line arguments for flexible settings/customization.

----------------------------------------------------------------------------------------------------

Copyright © 2020-2021 Sascha Offe \<so@saoe.net>  
SPDX-License-Identifier: [MIT](http://opensource.org/licenses/MIT)  
(See also LICENSES directory)
